.DEFAULT_GOAL := all

FILES :=                                  \
    .gitignore                            \
    .gitlab-ci.yml                        \
    collatz-tests                         \
    Collatz.py                            \
    makefile                              \
    RunCollatz.in                         \
    RunCollatz.out                        \
    RunCollatz.py                         \
    TestCollatz.py                        \
    #collatz-tests/ssun914-RunCollatz.in   \
    #collatz-tests/ssun914-RunCollatz.out  \
    Collatz.html                          \
    Collatz.log                           \

.pylintrc:
	pylint disable=missing-docstring, invalid-name --reports=no --generate-rcfile > $@

collatz-tests:
	git clone https://gitlab.com/gpdowning/cs373-collatz-tests.git collatz-tests

Collatz.html: Collatz.py
	-pydoc3 -w Collatz

Collatz.log:
	git log > Collatz.log

RunCollatz.pyx: Collatz.py RunCollatz.py .pylintrc
	-mypy   Collatz.py
	-pylint Collatz.py
	-mypy   RunCollatz.py
	-pylint RunCollatz.py
	./RunCollatz.py < RunCollatz.in > RunCollatz.tmp
	diff RunCollatz.tmp RunCollatz.out

TestCollatz.pyx: Collatz.py TestCollatz.py .pylintrc
	-mypy    Collatz.py
	-pylint  Collatz.py
	-mypy    TestCollatz.py
	-pylint  TestCollatz.py
	coverage run    --branch TestCollatz.py
	coverage report -m

all:

clean:
	rm -f  .coverage
	rm -f  .pylintrc
	rm -f  *.pyc
	rm -f  *.tmp
	rm -rf __pycache__
	rm -rf .mypy_cache

check: $(FILES)

config:
	git config -l

ctd:
	checktestdata RunCollatz.in.ctd  RunCollatz.in
	checktestdata RunCollatz.out.ctd RunCollatz.out

docker:
	docker run -it -v $(PWD):/usr/collatz -w /usr/collatz gpdowning/python

format:
	black Collatz.py
	black RunCollatz.py
	black TestCollatz.py

init:
	touch README
	git init
	git remote add origin git@gitlab.com:gpdowning/cs373-collatz.git
	git add README
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	-git add Collatz.html
	-git add Collatz.log
	git add Collatz.py
	git add makefile
	git add RunCollatz.in
	git add RunCollatz.in.ctd
	git add RunCollatz.out
	git add RunCollatz.out.ctd
	git add RunCollatz.py
	git add TestCollatz.py
	git commit -m "another commit"
	git push
	git status

run: RunCollatz.pyx TestCollatz.pyx

scrub:
	make clean
	rm -f  Collatz.html
	rm -f  Collatz.log
	rm -rf collatz-tests

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

versions:
	which         black
	black         --version
	@echo
	which         checktestdata
	checktestdata --version
	@echo
	which         coverage
	coverage      --version
	@echo
	which         git
	git           --version
	@echo
	which         make
	make          --version
	@echo
	which         mypy
	mypy          --version
	@echo
	which         pip
	pip           --version
	@echo
	which         pydoc
	pydoc         --version
	@echo
	which         pylint
	pylint        --version
	@echo
	which         python3
	python3       --version
	@echo
	which         vim
	vim           --version
